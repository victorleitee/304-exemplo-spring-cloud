package br.com.mastertech.imersivo.carro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {
	
	@Autowired
	private PagamentoRepository pagamentoRepository;

	public Iterable<Pagamento> obterPagamentos(Long id_cartao){
		return pagamentoRepository.findAllByPagamento_IdCartao(id_cartao);
	}
	public Pagamento novoPagamento(Pagamento pagamento) {
//		Pagamento pagamento = new Pagamento();
//		pagamento.setId(1);
//		pagamento.setCartao_id(cartaoId);
//		pagamento.setDescricao(descricao);
//		pagamento.setValor(valor);
		return pagamento;
	}
	
}
