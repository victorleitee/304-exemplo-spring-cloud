package br.com.mastertech.imersivo.carro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PagamentoController {
	
	@Autowired
	private PagamentoService pagamentoService;
	
	@GetMapping("/pagamento/{id_cartao}")
	public Iterable<Pagamento> obterPagamentos(@PathVariable Long id_cartao){
		return pagamentoService.obterPagamentos(id_cartao);
	}
	
	@PostMapping("/pagamento")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Pagamento novoPagamento(@RequestBody Pagamento pagamento) {
		return pagamentoService.novoPagamento(pagamento);
	}
}
