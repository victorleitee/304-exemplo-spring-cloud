package br.com.mastertech.imersivo.carro;

public class Pagamento {

	private long id;

	private long cartao_id;

	private String descricao;

	private double valor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCartao_id() {
		return cartao_id;
	}

	public void setCartao_id(long cartao_id) {
		this.cartao_id = cartao_id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

}
