package br.com.mastertech.imersivo.carro;

import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
	Iterable<Pagamento> findAllByPagamento_IdCartao(Long id_cartao);
}
