package br.com.mastertech.imersivo.pessoa;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "carro")
public interface PagamentoClient {
	
	@GetMapping("/carro/{modelo}")
	public Pagamento criaCarro(@PathVariable String modelo);
	
}	
