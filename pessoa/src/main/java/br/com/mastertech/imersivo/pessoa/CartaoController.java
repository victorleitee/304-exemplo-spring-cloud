package br.com.mastertech.imersivo.pessoa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CartaoController {
	
	@Autowired
	private CartaoService cartaoService;
	
	@GetMapping("/cartao/{nome}")
	public Cartao criaCartao(@PathVariable Long idCartao) {
		return cartaoService.criarCartao(idCartao);
	}

}
