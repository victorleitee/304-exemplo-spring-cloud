package br.com.mastertech.imersivo.pessoa;

public class Cartao {

	private long id;

	private long numero;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

}
